# Terpompilko

## Pri

Simpla ludo, simila al PONG.

Mi programis tiun ĉi por Ludfesto (http://www.nia-esperanto.com/)

## Kiel ludi

1. Elŝuti la projekton (.zip aŭ 'clone' per Git)
2. En la "Kodo" dosierujo, malfermi "ludu.html" paĝon.

## Kreinto

Programado kaj desegnado - Rachel J. Morris (http://www.moosader.com)

Sonoj - BFXR (http://www.bfxr.net/)

Kantoj - TGFCoder(http://art.devsader.com/)
